This is a repository for notes on Galois geometry.

# Elements of Finite Geometry
  by Steven H. Cullinane, last updated Jan. 17, 2016. 
  Downloadable compressed folder last updated Jan. 9, 2016.)"

A downloadable compressed folder of the source website finitegeometry.org/sc 
was last updated on Jan. 9, 2016. 
To download, click on http://finite-geometry.github.io/galois/finitegeometry-Jan-9-2016.zip .
For finite-geometry notes that are NOT compressed, see  
https://www.dropbox.com/sh/hh88rykuq5vpma2/AADLmYEqv4vz7Pajm9jxo3lUa?dl=0 .

This is a work under construction. 

- ### Preface — The nature of the work

    A preliminary version was distributed at the 1976 summer AMS-MAA joint 
    summer meetings in Toronto. Excerpts were published in the journal 
    Computer Graphics and Art (Vol. 2, No. 1, February 1977, pp. 5-7).
    
    Additional notes were distributed informally, and are available as 
    Notes on Groups and Geometry, 1978-1986 (pdf ). These and later notes are, 
    at this writing, on the Web at http://finitegeometry.org/sc/map.html. 
    This book is based on the notes at that site."

- ### Educational level

    This material is intended as a course supplement or independent-study 
    source for college students (undergraduate and graduate) who have taken 
    courses in abstract algebra (linear algebra and group theory) and modern 
    geometry (affine, projective, finite)
    
    "There is no royal road to geometry." 
    -- Saying attributed to Euclid
    
    The reader is not expected to fully understand this book unless he or she 
    has had undergraduate instruction in linear algebra, abstract algebra, and 
    modern geometry. The following references may, however, be helpful:
    
    Books:

    Symmetry, by Hermann Weyl

    Contemporary Abstract Algebra, by Joseph A. Gallian

    Geometry and Symmetry, by Paul B. Yale.
    
    It may also interest some professional mathematicians, since the content 
    has not been published previously except as follows."
    
- ### History

    - 1704- Truchet tiles
    - 1830- Galois fields
      "In 1830, Galois introduced finite (or, now, Galois) fields, a new type 
      of number systems that would later prove widely useful. "
    - 1832- Permutation groups
      "Galois also introduced permutation groups, a very basic and important 
      concept with which we assume the reader is familiar."
    - 1838- Froebel cubes
      " In 1838, educator Friedrich Fröbel introduced his "third gift," 
      a boxed set of eight identical cubical  unmarked kindergarten blocks."
    - 1892- Fano geometries
      "In 1892, Gino Fano introduced finite geometries. These were abstract 
      structures defined axiomatically. They were unrelated, so far as anyone 
      knew at the time, to any actual physical structures."
    - 1905- Hudson on Göpel and Rosenhain tetrads
      "In Kummer's Quartic Surface, Cambridge U. Press, 1905"
    - 1910- Conwell - "The 3-space PG(3,2) and its group"
    - 1912 and later- Coble - Algebraic geometry in light of finite geometry
      "1912- "An Application of Finite Geometry to the Characteristic Theory of 
      the Odd and Even Theta Functions""
    - 1938- Singer cycles are defined
      "J. Singer, "A theorem in finite projective geometry and some applications
      to number theory," Trans. Amer. Math. Soc. 43 (1938), 377-385."
    - 1945- Baer- "Null Systems in Projective Space"
    - 1968- Dembowski's Finite Geometries is published
    - 1974- Curtis submits his paper on the Miracle Octad Generator, "A New 
    Combinatorial Approach to M24"
    - 1976- The diamond theorem
      "The 1976 monograph and 1979 abstract"
  - Structures considered
    - The fourfold square — A 2x2 array of square unit cells
    - The eightfold cube — A 2x2x2 array of unit cubes
    - The 16fold square — A 4x4 array of square unit cells
    - The 16fold triangle — A triangular array of 16 triangular unit cells
    - The 24fold rectangle — A 4x6 array of square unit cells, split into 
    three 4x2 "bricks"
    - The 64fold cube — A 4x4x4 array of unit cubes

- ### Coordinates
    "Coordinates formed from the two-element Galois field GF(2) may be applied 
    to each of these in such a way that the resulting labeled structures have 
    large and interesting symmetry groups. In the case of the 4x6 array, it is 
    useful to view coordinates in each 4x2 brick not as 3-tuples formed from 
    GF(2), but rather as elements of the field GF(8)."